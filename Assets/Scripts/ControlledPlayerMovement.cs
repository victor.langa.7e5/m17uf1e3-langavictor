using UnityEngine;

public class ControlledPlayerMovement : MonoBehaviour
{
    private PlayerValuesManager _playerValuesManager;
    [SerializeField]
    private GameObject _distanceToTravel;

    private void OnEnable()=> _distanceToTravel.SetActive(false);
    private void Start()
    {
        _playerValuesManager = gameObject.GetComponent<PlayerValuesManager>();
    }

    void Update()
    {
        transform.position = new Vector3(
            transform.position.x + Input.GetAxis("Horizontal") * _playerValuesManager.GetCalculatedSpeed() * Time.deltaTime,
            transform.position.y);       
    }
}
