using UnityEngine;

public class AutonomousPlayerMovement : MonoBehaviour
{
    private PlayerValuesManager _playerValuesManager;
    [SerializeField]
    private GameObject _distanceToTravel;

    private float _speedSense = -1;
    private float _distanceTravelled;
    private float _waitTimeWhenDistanceAchieved;
    private float _waitTime;
    private bool _isPlayerWaiting = false;

    private void OnEnable() => _distanceToTravel.SetActive(true);

    private void Start()
    {
        _playerValuesManager = GetComponent<PlayerValuesManager>();
        _distanceTravelled = 0;
        _waitTimeWhenDistanceAchieved = 1;
        _waitTime = 0;
    }

    void Update()
    {
        if (_isPlayerWaiting) 
        {
            _waitTime += Time.deltaTime;
            if (_waitTime >= _waitTimeWhenDistanceAchieved) 
            {
                _speedSense *= -1;
                _distanceTravelled = 0;
                _waitTime = 0;
                _isPlayerWaiting = false;
            }
        }
        else
        {
            transform.position = new Vector3(
              transform.position.x + _playerValuesManager.GetCalculatedSpeed() * _speedSense * Time.deltaTime,
              transform.position.y);
            _distanceTravelled += _playerValuesManager.GetCalculatedSpeed() * Time.deltaTime; // Does not work properly if player collides in position.x
            _isPlayerWaiting = DistanceCheck();
        }
        
    }

    private bool DistanceCheck()
    {
        if (_distanceTravelled >= _playerValuesManager.distanceToTravel)
            return true;

        return false;
    }
}
