using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    private PlayerValuesManager _playerValuesManager;
    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private Text _playerUpName;
    [SerializeField]
    private Text _playerHudName;
    [SerializeField]
    private Text _fps;

    [SerializeField]
    private Scrollbar _speedScrollbar;
    [SerializeField]
    private Scrollbar _distanceScrollbar;
    [SerializeField]
    private Scrollbar _giantScrollbar;

    private float _counter;


    void Start()
    {
        _counter = 0;
        _playerValuesManager = GetComponent<PlayerValuesManager>();
        _fps.text = (int)(1 / Time.deltaTime) + "FPS";
    }

    void Update()
    {
        UpdatePlayerUpName();
        UpdateValuesFromScrollbars();
        _playerHudName.text = _playerValuesManager.playerName;

        _counter += Time.deltaTime;
        if (_counter >= 1)
        {
            _counter = 0;
            _fps.text = (int)(1 / Time.deltaTime) + "FPS";
        }
         
    }

    void UpdatePlayerUpName() 
    {
        _playerUpName.text = _playerValuesManager.playerName;
        _playerUpName.transform.position = _camera.WorldToScreenPoint(transform.position + new Vector3(0, transform.localScale.y, 0));
    }

    void UpdateValuesFromScrollbars() 
    {
        _playerValuesManager.weight = (float)(_playerValuesManager.GetWeightLimits()[1] - (_speedScrollbar.value * (_playerValuesManager.GetWeightLimits()[1] - _playerValuesManager.GetWeightLimits()[0])));
        _playerValuesManager.distanceToTravel = _playerValuesManager.GetMinDistanceToTravel() + (_distanceScrollbar.value * 100);
        _playerValuesManager.giantStateDuration = _playerValuesManager.GetMinGiantStateDuration() + (_giantScrollbar.value * 30);
    }
}
