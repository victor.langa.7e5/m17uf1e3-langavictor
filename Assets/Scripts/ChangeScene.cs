using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour
{
    [SerializeField]
    private InputField _inputField;
    [SerializeField]
    private Button _startGameButton;

    private void Start()
    {
        _startGameButton.onClick.AddListener( () => { StartGame(); } );
    }

    public void StartGame()
    {
        if (!CheckValidName())
        {
            _inputField.text = "";
            _inputField.placeholder.GetComponent<Text>().text = "Enter a valid name!";
            return;
        }

        PlayerPrefs.SetString("playerName", _inputField.text);
        SceneManager.LoadScene("Game");
    }

    private bool CheckValidName()
    {
        foreach (var character in _inputField.text)
            if (character != ' ')
                return true;

        return false;
    }
}
