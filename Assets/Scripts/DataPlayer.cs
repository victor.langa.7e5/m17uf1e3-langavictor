using UnityEngine;

public class DataPlayer : MonoBehaviour
{
    public enum PlayerType
    {
        Wanderer,
        Traveler,
        Patroller,
        Runner
    }

    public enum GiantState 
    { 
        NotActive,
        ProcState,
        Active,
        Cooldown
    }

    public struct PlayerValues
    {
        public string playerName;
        public PlayerType playerType;
        public double height;
        public float speed;
        public float distanceToTravel;
        public float giantStateDuration;
    }

    [SerializeField]
    private Sprite[] _sprites;
    private SpriteRenderer _spriteRenderer;
    private ControlledPlayerMovement _controlledPlayerMovement;
    private AutonomousPlayerMovement _autonomousPlayerMovement;

    private int _spriteArrayPosition = 0;
    private float _counter;
    private Vector3 _lastTransformPosition;
    private int movementSense = -1;
    private int _framesSinceLastSpriteUpdate;


    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _controlledPlayerMovement = GetComponent<ControlledPlayerMovement>();
        _autonomousPlayerMovement = GetComponent<AutonomousPlayerMovement>();

        _counter = 0;
        _framesSinceLastSpriteUpdate = 0;
    }

    void Update()
    {
        if (CheckIfMovement())
        {
            SetAnimationToConcretFramerate();
            FaceSpriteBasedOnSense();
        }
        if (Input.GetKeyDown(KeyCode.Tab)) ChangeMovementMode();
    }

    private bool CheckIfMovement()
    {
        if (_lastTransformPosition == transform.position)
        {
            _spriteRenderer.sprite = _sprites[1];
            return false;
        }

        return true;
    }

    private void SetAnimationToConcretFramerate()
    {
        float frameRate = 12;

        _counter += Time.deltaTime;
        if (_counter >= 1 / frameRate) // The sprite changes every frame of the framerate specified with little margin of error
        {
            _counter = 0;

            if (_framesSinceLastSpriteUpdate == 2)
            {
                _spriteArrayPosition = (_spriteArrayPosition + 1) % _sprites.Length;
                _spriteRenderer.sprite = _sprites[_spriteArrayPosition];
                _framesSinceLastSpriteUpdate = 0;
            }

            _framesSinceLastSpriteUpdate++;
        }
    }

    private void FaceSpriteBasedOnSense()
    {
        switch (movementSense)
        {
            case 1:
                movementSense *= ChangeRotationIfSenseChanged(_lastTransformPosition.x, transform.position.x);
                break;
            case -1:
                movementSense *= ChangeRotationIfSenseChanged(transform.position.x, _lastTransformPosition.x);
                break;
        }

        _lastTransformPosition = transform.position;
    }

    private int ChangeRotationIfSenseChanged(float position1, float position2)
    {
        if (position1 > position2)
        {
            transform.Rotate(0, 180, 0);
            return -1;
        }

        return 1;
    }

    private void ChangeMovementMode()
    {
        switch (_autonomousPlayerMovement.enabled)
        {
            case true:
                _autonomousPlayerMovement.enabled = false;
                break;
            case false:
                _autonomousPlayerMovement.enabled = true;
                break;
        }

        switch (_controlledPlayerMovement.enabled)
        {
            case true:
                _controlledPlayerMovement.enabled = false;
                break;
            case false:
                _controlledPlayerMovement.enabled = true;
                break;
        }
    }
}