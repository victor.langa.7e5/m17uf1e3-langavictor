using UnityEngine;

public class PlayerValuesManager : MonoBehaviour
{
    public string playerName;
    public DataPlayer.PlayerType playerType;
    public double height;
    public double weight;
    public float speed;
    public float distanceToTravel;
    public float giantStateDuration;

    private double[] heightLimits = new[] { 0.5, 2.5 };
    private double[] weightLimits = new[] { 20.0, 100.0 };
    private float minDistanceToTravel = 1f;
    private float minGiantStateDuration = 1f;

    private void Start()
    {
        playerName = PlayerPrefs.GetString("playerName");
        playerType = DataPlayer.PlayerType.Wanderer;
        height = 1.5;
        weight = 60;
        speed = GetCalculatedSpeed();
        distanceToTravel = 10;
        giantStateDuration = 10;
    }

    private void Update()
    {
        if (playerName.Length == 0) playerName = "DefaultName";
        if (playerName.Length > 15) playerName = playerName.Substring(0, 15);
        if (height < heightLimits[0]) height = heightLimits[0];
        if (height > heightLimits[1]) height = heightLimits[1];
        if (weight < weightLimits[0]) weight = weightLimits[0];
        if (weight > weightLimits[1]) weight = weightLimits[1];
        if (minDistanceToTravel <= 0) distanceToTravel = minDistanceToTravel;
        if (minGiantStateDuration < 1) giantStateDuration = minGiantStateDuration;
        speed = GetCalculatedSpeed();
    }

    public float GetCalculatedSpeed()
    {
        return 150 / (float)weight;
    }

    public float GetGiantStateDuration()
    {
        return giantStateDuration;
    }

    public double[] GetWeightLimits()
    {
        return weightLimits;
    }

    public float GetMinDistanceToTravel()
    {
        return minDistanceToTravel;
    }

    public float GetMinGiantStateDuration()
    {
        return minGiantStateDuration;
    }
}
