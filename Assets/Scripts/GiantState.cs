using UnityEngine;

public class GiantState : MonoBehaviour
{
    private PlayerValuesManager _playerValuesManager;
    private DataPlayer.GiantState _giantState;
    private float _giantStateTimer;
    private float _giantStateDuration;

    void Start()
    {
        _giantState = DataPlayer.GiantState.NotActive;
        _giantStateTimer = 0;
        _playerValuesManager = gameObject.GetComponent<PlayerValuesManager>();
    }

    void Update()
    {
        _giantStateDuration = _playerValuesManager.GetGiantStateDuration();
        UpdateGiantState();

        if (Input.GetKeyDown(KeyCode.LeftShift) && _giantState == DataPlayer.GiantState.NotActive)
            _giantState = DataPlayer.GiantState.ProcState;
    }

    private void UpdateGiantState()
    {
        switch (_giantState)
        {
            case DataPlayer.GiantState.ProcState:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer < _playerValuesManager.height)
                    transform.localScale += new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime);
                else
                {
                    _giantStateTimer = 0;
                    _giantState = DataPlayer.GiantState.Active;
                }
                break;
            case DataPlayer.GiantState.Active:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer >= _giantStateDuration)
                {
                    _giantStateTimer = 0;
                    _giantState = DataPlayer.GiantState.Cooldown;
                }
                break;
            case DataPlayer.GiantState.Cooldown:
                _giantStateTimer += Time.deltaTime;
                if (_giantStateTimer < _playerValuesManager.height)
                    transform.localScale -= new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime);
                if (_giantStateTimer >= 30)
                {
                    _giantStateTimer = 0;
                    _giantState = DataPlayer.GiantState.NotActive;
                }
                break;
        }
    }
}
